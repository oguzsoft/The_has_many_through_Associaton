class Floor < ApplicationRecord
  has_many :apartments
  belongs_to :build
end
