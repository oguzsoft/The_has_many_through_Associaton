class Build < ApplicationRecord
  has_many :floors
  has_many :apartments, :through => :floors
end
