json.extract! apartment, :id, :gate_number, :created_at, :updated_at
json.url apartment_url(apartment, format: :json)
