class AddBuildToApartment < ActiveRecord::Migration[5.2]
  def change
    add_reference :apartments, :build, foreign_key: true
  end
end
