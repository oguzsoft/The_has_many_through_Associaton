class CreateApartments < ActiveRecord::Migration[5.2]
  def change
    create_table :apartments do |t|
      t.string :gate_number

      t.timestamps
    end
  end
end
