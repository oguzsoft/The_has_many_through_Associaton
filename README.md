## has_many, through Association
 * Create a rails app

    ```rails new building -d postgresql -B -T ```

 * Open app and go to Gemfile

    ```cd building ```

    _add those lines in Gemfile_
    ```
      gem 'hirb'
      gem 'simple_form'
    ```

* Run bundle command ```bundle install```
* Run ```rails g simple_form:install``` command 

_**For example, consider a build where floors to see apartments. Your models will see below like;**_

![](build.png)

## Scaffolding

  ```rails g scaffold Build name:string```

  ```rails g scaffold Floor floor_number:string```

  ```rails g scaffold Apartment gate_number:string```

## Migration

  ```rails g migration add_build_to_floor build:references```

  ```rails g migration add_floor_to_apartment floor:references```

  ```rails g migration add_build_to_apartment build:references```

* Create database, run ```rails db:create ```

* Migrate run ```run db:migrate```

* Setup database run ```rails db:setup```

* **in models/apartment.rb**
  ```
    class Apartment < ApplicationRecord
      belongs_to :floor
      belongs_to :build
    end
  ```
* **in models/build.rb**
  ```
    class Build < ApplicationRecord
      has_many :floors
      has_many :apartments, :through => :floors
    end
  ```
* **in models/floor.rb**
  ```
    class Floor < ApplicationRecord
      belongs_to :build
      has_many :apartments
    end
  ```

* **in views/apartments/_ form.html.erb**
  * _added those lines_
    ```
      <%= f.association :floor, label_method: :floor_number %>
      <%= f.association :build %>
    ```
  * _Your form look like this_

    ```

      <%= simple_form_for(@apartment) do |f| %>
        <%= f.error_notification %>
        <%= f.error_notification message: f.object.errors[:base].to_sentence if f.object.errors[:base].present? %>

        <div class="form-inputs">
          <%= f.input :gate_number %>
          <%= f.association :floor, label_method: :floor_number %>
          <%= f.association :build %>
        </div>

        <div class="form-actions">
          <%= f.button :submit %>
        </div>
      <% end %>
    ```



* **in views/apartments/index.html.erb**
  * _added those lines for display the apartment floor_number and build name_
    ```
      <td><%= apartment.floor.floor_number %></td>
      <td><%= apartment.build.name %></td>
    ```
  * _Your index look like this;_

    ```
      <p id="notice"><%= notice %></p>

      <h1>Apartments</h1>

      <table>
        <thead>
          <tr>
            <th>Gate number</th>
            <th>Floor</th>
            <th>Build</th>
            <th colspan="3"></th>
          </tr>
        </thead>

        <tbody>
          <% @apartments.each do |apartment| %>
            <tr>
              <td><%= apartment.gate_number %></td>
              <td><%= apartment.floor.floor_number %></td>
              <td><%= apartment.build.name %></td>
              <td><%= link_to 'Show', apartment %></td>
              <td><%= link_to 'Edit', edit_apartment_path(apartment) %></td>
              <td><%= link_to 'Destroy', apartment, method: :delete, data: { confirm: 'Are you sure?' } %></td>
            </tr>
          <% end %>
        </tbody>
      </table>

      <br>

      <%= link_to 'New Apartment', new_apartment_path %>
    ```


* **in controllers/apartments_controller.rb**
  * _added ```:floor_id, :build_id``` to 'apartment_params' method for save apartment. Your aparment_params method look like this;_
    ```
      def apartment_params
        params.require(:apartment).permit(:gate_number, :floor_id, :build_id)
      end
    ```




















